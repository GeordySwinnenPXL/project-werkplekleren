let mailto =  sessionStorage.getItem('userid');
function postEmail() {

    const mailsubject = "Wachtwoord Aanpassen";
    const mailbody = "We hebben vernomen dat u uw wachtwoord wilt aanpassen. Via volgende link kunt u dit doen http://localhost:63342/pro-g/BitBucket/project-werkplekleren/pswdchange.html?_ijt=aoc3go1d9gccnrhs2temhe1pmj";
    console.log(mailto);
    const data = {
        mailto: mailto,
        mailsubject: mailsubject,
        mailbody: mailbody,
    }
    fetch('https://localhost:44399/api/Mail/',  {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    })
        .then(validateResponse)
        .then(logResult)
        .catch(logError);
}