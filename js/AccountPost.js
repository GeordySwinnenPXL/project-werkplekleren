window.addEventListener("load", loaded)
let spanErrorMessage;

function loaded() {
let registerButton = document.getElementById('registratie');
registerButton.addEventListener("click" ,postRequest);
spanErrorMessage = document.getElementById("Errorm")
}

function postRequest() {
    const email = document.getElementById("txtEmail").value;
    const pwd = document.getElementById("txtPwd").value;
    const firstname = document.getElementById("txtFirst").value;
    const lastname = document.getElementById("txtLast").value;
    const birthdate = document.getElementById("txtBirth").value;
    const data = {
        firstName: firstname,
        lastName: lastname,
        birthDate: birthdate,
        email: email,
        password: pwd,
    }

    if (!checkName(firstname, lastname)){
        return;
    }

    if (!checkPassw(pwd))
    {
        return;
    }

    //fetch('https://localhost:44397/api/Account/CreateAccount/', {
    fetch('https://localhost:44399/api/Account/', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    }).then(validateResponse)
        .then(logResult)
        .then(logData)
        .catch(logError);
}

function logResult(result) {
    return result.json();
}

function logData(result) { // logData will log the response from the server
    console.log(result.data)
}
function logError(error) {
    console.log('Looks like there was a problem:', error);
}
function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

function checkName(Fname, Lname)
{
    if (Fname.trim() === "" ){
        spanErrorMessage.setAttribute( "style", "display:block" );
        spanErrorMessage.textContent = "Gelieve je naam in te geven";
    }else if (Lname.trim() === ""){
        spanErrorMessage.setAttribute( "style", "display:block" );
        spanErrorMessage.textContent = "Gelieve je achternaam in te geven";
    }else {
        spanErrorMessage.setAttribute( "style", "display: none;" );
        return true;
    }
}

function checkPassw(Passw)
{
    var passw = /^[A-Za-z]\w{7,14}$/;
    if (Passw.trim() === ""){
        spanErrorMessage.setAttribute( "style", "display:block" );
        spanErrorMessage.textContent = "Gelieve een passwoord met minimaal 6 tekens in te geven";
    } if (Passw.length < 6){
    spanErrorMessage.setAttribute( "style", "display:block" );
    spanErrorMessage.textContent = "Gelieve een passwoord met minimaal 6 tekens in te geven";
    } else
    {
        spanErrorMessage.setAttribute( "style", "display: none;" );
        return true;
    }
}