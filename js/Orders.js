window.addEventListener("load", loaded);

function loaded() {
    GetOrders()
}



function GetOrders() {
    if (sessionStorage.getItem('userid') !== null){
    let url = 'https://localhost:44399/api/Order?userid=' + sessionStorage.getItem('userid') + '';
    fetch(url,
        {
            method: "GET",
            headers: {
                'accept': 'application/json',
                'content-Type': 'application/json',
            }
        }).then((response) => {
        if (response.ok) {
            console.log("succes");
            return response.json();


        } else {
            console.log("Something went wrong!");
        }
    })
        .then((response) => {
                console.log(response);

            let pageNumber = sessionStorage.getItem('pagenumber2');
            if (sessionStorage.getItem('pagenumber2') == null) {
                pageNumber = 1
            }
            sessionStorage.setItem('pagenumber2', pageNumber);
            document.getElementById("pagenumber2").innerHTML = pageNumber;
            document.getElementById("pagenumberu2").innerHTML = pageNumber;
            let page = parseInt(pageNumber);
            if (page === 1) {
                document.getElementById("page2u").style.display = "none";
                document.getElementById("page2").style.display = "none";
                document.getElementById("page3").innerHTML = page + 1;
                document.getElementById("page4").innerHTML = page + 2;
                document.getElementById("page3u").innerHTML = page + 1;
                document.getElementById("page4u").innerHTML = page + 2;
            } else {

                document.getElementById("page2").innerHTML = page - 1;
                document.getElementById("page3").innerHTML = page + 1;
                document.getElementById("page2u").innerHTML = page - 1;
                document.getElementById("page3u").innerHTML = page + 1;
                document.getElementById("page4u").style.display = "none";
                document.getElementById("page4").style.display = "none";

            }

                let obj = JSON.parse(response.data);
                console.log(obj);
                for (let i = ((pageNumber - 1) * 10); i < 10 * pageNumber; i++) {

                    let productid = obj[i].productid;
                    let prodimg = obj[i].productid + ".png";
                    let producttitle = obj[i].producttitle;
                    let productprijs = obj[i].price;
                    let productamount = obj[i].amount;
                    let orderid = obj[i].orderid;
                    let prijs = productprijs.replace(',','.');
                    let totaalprijs = prijs * productamount;

                    let orderitem = `

     <div id="${productid}" class="bestelling">
        <img class="productimg" src="../project-werkplekleren/img/afbeeldingen/${prodimg}">
        <div class="orderinfo">
        <p><b>Orderid: </b> ${orderid}</p>
        <p><b>Productnaam: </b>${producttitle}</p>
        <p <a id="aantal"><b>Aantal:</b> ${productamount}</a><a id="prijs"><b>Prijs per stuk: </b>€${productprijs}</a><a class="totaalprijs"><b>Totaalprijs:</b> €${totaalprijs}</a></p>
        </div>
     </div>  
        
     `;
                    let li = document.createElement('li');
                    const orderlist = document.getElementById('bestellingen');
                    li.classList.add('order-row');
                    li.innerHTML = orderitem;
                    orderlist.append(li);
                }
            }
        );
    }
}


function pageup() {
    let pageNumber = sessionStorage.getItem('pagenumber');
    pageNumber++;
    sessionStorage.setItem('pagenumber', pageNumber);
    location.reload();
    console.log(pageNumber)
}

function gotopage(page) {
    console.log(page)

    let pageNumber = document.getElementById(page).innerHTML
    sessionStorage.setItem('pagenumber', pageNumber);
    location.reload();
    console.log(pageNumber)
}

function pagedown() {
    let pageNumber = sessionStorage.getItem('pagenumber');
    if (pageNumber>0){
        console.log(pageNumber)
        pageNumber = pageNumber-1;
        sessionStorage.setItem('pagenumber', pageNumber);
        location.reload();
    }

}