function postEmail() {

    const mailto = document.getElementById("txtemailto").value;
    const mailsubject = document.getElementById("txtsubject").value;
    const mailbody = document.getElementById("txtbody").value;
    const data = {
        mailto: mailto,
        mailsubject: mailsubject,
        mailbody: mailbody,
    }
    fetch('https://localhost:44397/api/Mail/',  {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data),
    })
        .then(validateResponse)
        .then(logResult)
        .catch(logError);
}
function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
function logResult(result) {
    console.log(result.data);
    console.log(JSON.parse(result.data))
}
function logError(error) {
    console.log('Looks like there was a problem:', error);
}