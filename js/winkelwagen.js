window.addEventListener("load", loaded);

function loaded(){
    GetItemse();
}

function addtocart(id) {
    console.log(id);

    let cartRow = document.createElement('div');
    let prodimg = document.getElementById(id).getElementsByClassName('productimg')[0].src;
    let producttitle = document.getElementById(id).getElementsByTagName('p')[0].innerHTML.trim();
    let productprijs = document.getElementById(id).getElementsByTagName('p')[1].innerHTML.split("€")[1].split('<input')[0]
    let amount = '1';
    let shopitem = `

     <div id="${id}" class="shopitemdiv">
        <img class="shopitemimg" src="${prodimg}">
        <div class="shopitemtitle"><p>${producttitle}</p></div>
     <input id="aantal" class="shopitemamout" type="number" value="${amount}">
    <p class="shopitemprijs">${productprijs}</p>
     <a id="${id}" class="removeshopitem" onclick="DeleteCartItem(this.id)">X</a>
        </div>
        
     
     `;
    let shoppingcar = document.getElementsByClassName('cart-items')[0];

    cartRow.classList.add('cart-row');
    cartRow.innerHTML = shopitem;
    cartRow.getElementsByClassName('shopitemamout')[0].addEventListener('change', quantityChanged);
    cartRow.getElementsByClassName('removeshopitem')[0].addEventListener('click', removeCartItem);
    ShoppingCartHandler(id);
    shoppingcar.append(cartRow);
    updateCartTotal();

}



function removeCartItem(event) {
    let buttonClicked = event.target;
    buttonClicked.parentElement.parentElement.remove();
    updateCartTotal()
}

function updateCartTotal() {


    let cartItemContainer = document.getElementsByClassName('cart-items')[0];
    let cartRows = cartItemContainer.getElementsByClassName('cart-row');

    let total = 0;
    for (let i = 0; i < cartRows.length; i++) {
        let cartRow = cartRows[i];
        let priceElement = cartRow.getElementsByClassName('shopitemprijs')[0];
        let quantityElement = cartRow.getElementsByClassName('shopitemamout')[0];
        let price = parseFloat(priceElement.innerText.replace('€', ''));
        let quantity = quantityElement.value;
        total = total + (price * quantity)
    }
    total = Math.round(total * 100) / 100;
    document.getElementsByClassName('item-total')[0].innerText = '€' + total;
    calculateAantal();
}

function calculateAantal() {
    let aantal = 0;
    let elem = document.getElementsByClassName("shopitemamout");
    let inputs = [];
    for (var i = 0; i < elem.length; ++i) {
        if (typeof elem[i].value !== "undefined") {
            inputs.push(elem[i].value);
        }

    }

    for (var f = 0; f< inputs.length; ++f){
        aantal = aantal + parseInt(inputs[f]);
    }

    document.getElementById("item-count").innerHTML = aantal;
    console.log(aantal)
    tellers = aantal

}

function quantityChanged(event) {
    var input = event.target;
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal();
    amountchanged(input.value)
}


function amountchanged(aantal) {
    let productid = document.getElementById('aantal').parentElement.id;
    let amount = aantal
    let userid = sessionStorage.getItem('userid');
    if (userid != null){
        fetch('https://localhost:44399/api/Order?userid=' + localStorage.getItem("userid") + '&productid=' + productid + '&amount='+ amount + '',{
            method: 'PUT',
        })
            .then((response) => response.json())
            .then((result) => {
                console.log('Success:', result);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }
}
window.addEventListener("beforeunload", sessionstorageitems);
var tellers;
function sessionstorageitems() {
    localStorage.removeItem('carcontent');
    console.log("sesionstorage:" + tellers)
    var lengte = document.getElementsByClassName('shopitemdiv').length;
    var arr = new Array(lengte);

    for (var i = 0; i < arr.length; i++) {
        arr[i] = new Array(5);
    }
    for (var w = 0; w < arr.length; w++) {
        var item = document.getElementsByClassName('shopitemdiv')[w];
        let productid = item.id;
        var img = item.getElementsByClassName('shopitemimg')[0].src;
        var title = item.getElementsByClassName("shopitemtitle")[0].getElementsByTagName('p')[0].innerHTML;
        var aantal = item.getElementsByClassName('shopitemamout')[0].value;
        var prijs = item.getElementsByClassName('shopitemprijs')[0].innerHTML;
        arr[w][0] = img;
        arr[w][1] = title;
        arr[w][2] = aantal;
        arr[w][3] = prijs;
        arr[w][4] = productid;
    }
    localStorage.setItem('carcontent', JSON.stringify(arr));
    console.log(localStorage.getItem('carcontent'));

}


function DeleteCartItem(prodid) {
    let userid = sessionStorage.getItem('userid');
    if (userid !== null){

        return fetch('https://localhost:44399/api/Order?userid=' + userid + "&productid=" + prodid + '' , {
            method: 'delete',
            headers: {
                'accept': 'application/json',
                'content-Type': 'application/json',
            }
        })
    }
}

function ShoppingCartHandler(prodid)
{
    let userid = sessionStorage.getItem('userid');

    if (userid !== null){
        let amount = '1';
        let CartModel = {userid: userid, products: prodid, amounts: amount};

        fetch( 'https://localhost:44399/api/Order/',
            {
                method: "post",
                body: JSON.stringify( CartModel ),
                headers: {
                    'Content-Type': 'application/json',
                }
            } ).then(validateResponse)
            .then(logResult)
            .then(logData)
            .catch(logError);
    }
}


function logResult(result) {
    return result.json();

}

function logData(result) {
    console.log(result.data);


}
function logError(error) {
    console.log('Looks like there was a problem:', error);
}
function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

function GetItemse() {
    console.log("triggered");
    /*console.log(sessionStorage.getItem("userid"));
    if (sessionStorage.getItem("userid")===null){
        console.log("beforeloop")*/
        var producten = JSON.parse(localStorage.getItem('carcontent'));
        console.log(producten);
        for (var i = 0; i < producten.length; i++) {
            var cartRow = document.createElement('div');
            var prodimg = producten[i][0];
            var producttitle = producten[i][1];
            var amount = producten[i][2];
            var productprijs = producten[i][3];
            let id = producten[i][4];

            var shopitem = `

     <div id="${id}" class="shopitemdiv">
        <img class="shopitemimg" src="${prodimg}">
        <div class="shopitemtitle"><p>${producttitle}</p></div>
     <input id="aantal" class="shopitemamout" type="number" value="${amount}">
    <p class="shopitemprijs">${productprijs}</p>
     <a id="${id}" class="removeshopitem" onclick="DeleteCartItem(this.id)">X</a>
        </div>
     `;
            var shoppingcar = document.getElementsByClassName('cart-items')[0];
            cartRow.classList.add('cart-row');
            cartRow.innerHTML = shopitem;
            cartRow.getElementsByClassName('shopitemamout')[0].addEventListener('change', quantityChanged);
            cartRow.getElementsByClassName('removeshopitem')[0].addEventListener('click', removeCartItem);
            shoppingcar.appendChild(cartRow);
            updateCartTotal()
        }
    /*}*/
}



/*
const cartLightbox = document.querySelector(".lightbox-container");
const storeItems = document.querySelector('#store-items');
const productData = [{id:1,name:'cola',price:1},{id:2,name:'jupiler',price:3.99},{id:3,name:'fanta',price:5}];
let shoppingCart = [];
window.onload = function(){
    productData.forEach(product =>{
        let container = document.createElement('div');
        let lblName = document.createElement('label');
        let lblPrice = document.createElement('label');
        let btnAddToCart = document.createElement('input');
        lblName.innerText = 'product: ' + product.name;
        lblPrice.innerText = 'prijs: ' + product.price;
        btnAddToCart.type = 'button';
        btnAddToCart.addEventListener('click', function(){
            let productInCart = shoppingCart.find(prod => prod.id === product.id);
            if(productInCart){
                productInCart.quantity += 1;
            }else{
                product.quantity = 1;
                shoppingCart.push(product)
            }
            console.log('shopping cart', shoppingCart)
        });
        container.appendChild(lblName);
        container.appendChild(lblPrice);
        container.appendChild(btnAddToCart);
        storeItems.appendChild(container);
    });
};
function openLightbox(){
    cartLightbox.style.display = "flex";
}
function closeLightbox(){
    cartLightbox.style.display = "none";
};
// show cart
(function () {
    const cartIinfo = document.getElementById('cart-info');
    const cart = document.getElementById('cart');
    cartIinfo.addEventListener("click", function () {
        cart.classList.toggle("show-cart");
    });
})();
// add items to cart
(function () {
    const cartBtn = document.querySelectorAll('.store-item-icon');
    cartBtn.forEach(function (btn) {
        btn.addEventListener('clicker', function (event) {
            //console.log(event.target);
            if (event.target.parentElement.classList.contains('store-item-icon')) {
                let fullpath = event.target.parentElement.previousElementSibling.src;
                let pos = fullpath.indexOf("img") + 3;
                let partPath = fullpath.slice(pos);
                const item = {};
                item.img = `img-cart${partPath}`;
                let naam = event.target.parentElement.parentElement.nextElementSibling
                    .children[0].children[0].textContent;
                item.name = name;
                let price = event.target.parentElement.parentElement.nextElementSibling
                    .children[0].children[1].textContent;
                let laatsteprijs = price.slice(1).trim();
                item.price = finalPrice;

                const cartItem = document.createElement('div');
                cartItem.classList.add("cart-item", "d-flex", "justify-content-between", "text-capitalize", "my-3");
                cartItem.innerHTML = `
      <div class="cart-item d-flex justify-content-between text-capitalize my-3">
              <img src="${item.img}" class="img-fluid rounded-circle" id="item-img" alt="">
              <div class="item-text">
              <p id="cart-item-title" class="font-weight-bold mb-0">${item.name}</p>
          <span>$</span>
          <span id="cart-item-price" class="cart-item-price" class="mb-0">${item.price}</span>
              </div>
              <a href="#" id='cart-item-remove' class="cart-item-remove">
              <i class="fas fa-trash"></i>
              </a>
              </div>
              `;
                //selet cart
                const cart = document.getElementById('cart');
                const total = document.querySelector('cart-total-container');
                cart.insertBefore(cartItem, total);
                alert("item added to the cart");
                showTotals();
            }
        });
    });

    //show totals
    function showTotals() {
        const total = [];
        const items = document.querySelectorAll(".cart-item-price");
        items.foreach(function (item) {
            total.push(parseFloat(item.textContent));
        });
        const totalMoney = toal.reduce(function (total, item) {
            total += item;
            return total;
        }, 0);
        const finalMoney = totalMoney.toFixed(2);
        document.getElementById('cart-total').textContent = finalMoney;
        document.getElementById('item-total').textContent = finalMoney;
        document.getElementById('item-count').textContent = total.length;
    }
})();
*/


