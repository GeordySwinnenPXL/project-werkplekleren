function updatepswd(){

    let paswd = document.getElementById("pswd").value;
    let repeatpassword = document.getElementById("Opnieuwpwd").value;

    if (!checkPassword( paswd )) {
        return;
    }

    if (!checkRepeatpaswd(paswd, repeatpassword)){
        return;
    }


    fetch('https://localhost:44399/api/Password?userid=' + localStorage.getItem('userid') + '&password=' + paswd + '',{
        method: 'PUT',

    })
        .then((response) => response.json())
        .then((result) => {
            console.log('Success:', result);
        }).then(sluit())
        .catch((error) => {
            console.error('Error:', error);
        });
}
function sluit() {
    document.getElementById("Errorreg3").style.display = "block";
    setTimeout(function(){
        window.close()}, 3000);
}

function checkPassword(password) {
    let hasUppercase = false;
    let hasNumber = false;
    let hasLowercase = false;
    let hasSymbol = false;

    if (password.length < 6) {

        document.getElementById("Errorreg").style.display = "block";
        return false;
    } else {
        for (let i = 0; i < password.length; i++) {
            if (password.charCodeAt( i ) < 91 && password.charCodeAt( i ) > 64) {
                hasUppercase = true;
            }

            if (password.charCodeAt( i ) < 123 && password.charCodeAt( i ) > 96) {
                hasLowercase = true;
            }

            if (password.charCodeAt( i ) < 58 && password.charCodeAt( i ) > 47) {
                hasNumber = true;
            }

            if (password.charCodeAt( i ) > 34 && password.charCodeAt( i ) < 38) {
                hasSymbol = true;
            }
        }

        if (!hasUppercase) {
            document.getElementById("Errorreg").style.display = "block";
            return false;
        }

        if (!hasNumber) {
            document.getElementById("Errorreg").style.display = "block";
            return false;
        }

        if (!hasLowercase) {
            document.getElementById("Errorreg").style.display = "block";
            return false;
        }

        if (!hasSymbol) {
            document.getElementById("Errorreg").style.display = "block";
            return false;
        }
    }
    document.getElementById("Errorreg").style.display = "none";
    return true;
}

function checkRepeatpaswd(psw, rpsw){
    if (psw !== rpsw){

        document.getElementById("Errorreg2").style.display = "block";
    }else {
        document.getElementById("Errorreg2").style.display = "none";
        return true;
    }
}