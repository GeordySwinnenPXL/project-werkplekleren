window.addEventListener("load", loaded);

function loaded() {
    GetProducts();
    Admindisplay();
}
function Admindisplay(){
    if (sessionStorage.getItem('userid') == '1'){
        document.getElementById("Admin").style.display = "inline"
    }
    else{
        document.getElementById("Admin").style.display = "none"
    }
}
let producten =[];
let subcategorie = "";
function GetProducts() {
    let url = 'https://localhost:44399/Api/Product';
    fetch(url,
        {
            method: "GET",
            headers: {
                'accept': 'application/json',
                'content-Type': 'application/json',
            }
        }).then((response) => {
        if (response.ok) {
            console.log("succes");
            return response.json();

        } else {
            console.log("Something went wrong!");
        }
    })
        .then((response) => {
                console.log(response);
                let pageNumber = sessionStorage.getItem('pagenumber2');
                if (sessionStorage.getItem('pagenumber2') == null) {
                    pageNumber = 1
                }
                sessionStorage.setItem('pagenumber2', pageNumber);
                document.getElementById("pagenumber2").innerHTML = pageNumber;
                document.getElementById("pagenumberu2").innerHTML = pageNumber;
                let page = parseInt(pageNumber);
                if (page === 1) {
                    document.getElementById("page2u").style.display = "none";
                    document.getElementById("page2").style.display = "none";
                    document.getElementById("page3").innerHTML = page + 1;
                    document.getElementById("page4").innerHTML = page + 2;
                    document.getElementById("page3u").innerHTML = page + 1;
                    document.getElementById("page4u").innerHTML = page + 2;
                } else {

                    document.getElementById("page2").innerHTML = page - 1;
                    document.getElementById("page3").innerHTML = page + 1;
                    document.getElementById("page2u").innerHTML = page - 1;
                    document.getElementById("page3u").innerHTML = page + 1;
                    document.getElementById("page4u").style.display = "none";
                    document.getElementById("page4").style.display = "none";

                }

                let obj = JSON.parse(response.data);
                console.log(obj);
                for (let i = ((pageNumber - 1) * 309); i < 309 * pageNumber; i++) {

                    let productid = obj[i].productid;
                    let prodimg = "../project-werkplekleren/img/afbeeldingen/" + obj[i].productid + ".png";

                    let merk = obj[i].merk;
                    let productnaam = obj[i].productname;
                    let productprijs = obj[i].price;
                    let hoeveelheid = obj[i].hoeveelheid;
                    let alcoholpercentage = obj[i].alcohol;
                    let description = obj[i].description;
                    let subcategorie = obj[i].subcategorie;
                    let orderitem = `

     <div id="${productid}" class="Assortiment">
        <img class="productimg" src="${prodimg}">
        <div class="assortimentinfo">
        <p>${merk} ${productnaam}</p>
        <p>${hoeveelheid} €${productprijs} <input id="${productid}" class="Winkelen" type="button" onclick="addtocart(this.id)" name="add" value="Add"></p>
        </div>
        
     </div>        
     `;
                    let li = document.createElement('li');
                    const assortiment= document.getElementById('assort');
                    li.classList.add('assortiment-prod');
                    li.innerHTML = orderitem;
                    assortiment.append(li);


                    producten = obj;
                }

            }
        );
}


function filterProducts(subcat){
    let filterproduct = producten;
    subcategorie = subcat;
    showProd(filterproduct);
    renderProducts(filterproduct);
}
function showProd(filterproduct){
    return filterproduct.filter(product => producten.subcategorie === subcategorie);
}
function renderProducts(products){
    const assortiment = document.getElementById('assort');
    assortiment.innerHTML="";
    products.forEach(product => {
        console.log(product);
        let productid = product.productid;
        let prodimg = "../project-werkplekleren/img/afbeeldingen/" + product.productid + ".png";

        let merk = product.merk;
        let productnaam = product.productname;
        let productprijs = product.price;
        let hoeveelheid = product.hoeveelheid;
        let alcoholpercentage = product.alcohol;
        let description = product.description;
        let subcategorie = product.subcategorie;
        let orderitem = `

     <div id="${productid}" class="Assortiment">
        <img class="productimg" src="${prodimg}">
        <div class="assortimentinfo">
        <p>${merk} ${productnaam}</p>
        <p>${hoeveelheid} €${productprijs} <input type="text" class="prodamount" id="${productid}" value="1" width="50px"></p>
        <p><input id="${productid}" class="Winkelen" type="button" onclick="ShoppingCartHandler(this.id)" name="add" value="Add"></p>
        </div>
        
     </div>        
     `;

        let li = document.createElement('li');
        const assortiment = document.getElementById('assort');
        assortiment.innerHTML = "";
        li.classList.add('assortiment-prod');
        li.innerHTML = orderitem;
        assortiment.append(li);
    })
}



function ShoppingCartHandler(prodid)
{
    let userid = sessionStorage.getItem('userid');

    if (userid !== null){
        let amount = document.getElementById(prodid).getElementsByClassName('prodamount')[0].value;
        let CartModel = {userid: userid, products: prodid, amounts: amount};

        fetch( 'https://localhost:44399/api/Order/',
            {
                method: "post",
                body: JSON.stringify( CartModel ),
                headers: {
                    'Content-Type': 'application/json',
                }
            } ).then(validateoResponse)
            .then(logoResult)
            .then(logoData)
            .catch(logoError);
    }
}


function pageup() {
    let pageNumber = sessionStorage.getItem('pagenumber');
    pageNumber++;
    sessionStorage.setItem('pagenumber', pageNumber);
    location.reload();
    console.log(pageNumber)
}

function gotopage(page) {
    console.log(page);

    let pageNumber = document.getElementById(page).innerHTML;
    sessionStorage.setItem('pagenumber', pageNumber);
    location.reload();
    console.log(pageNumber)
}

function pagedown() {
    let pageNumber = sessionStorage.getItem('pagenumber');
    if (pageNumber>0){
        console.log(pageNumber);
        pageNumber = pageNumber-1;
        sessionStorage.setItem('pagenumber', pageNumber);
        location.reload();
    }

}

function logoResult(result) {
    return result.json();

}

function logoData(result) {
    console.log(result.data);


}
function logoError(error) {
    console.log('Looks like there was a problem:', error);
}
function validateoResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

