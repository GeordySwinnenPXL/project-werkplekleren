window.addEventListener("load", loaded);

function loaded() {
    Logdisplay();
}

function Logdisplay(){
    if (sessionStorage.getItem('userid') !== null){
        document.getElementById("logoutdiv").style.display = "inline"
        document.getElementById("loginbut").style.display = "none"
    }

}

function logouthandler(){
    document.getElementById("logoutdiv").style.display = "none"
    document.getElementById("loginbut").style.display = "inline"
    sessionStorage.clear();
}