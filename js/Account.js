window.addEventListener("load", loaded);
function loaded() {
    getAccount();
    Admindisplay();
}
function Admindisplay(){
    if (sessionStorage.getItem('userid') == '1'){
        document.getElementById("Admin").style.display = "inline"
    }
    else{
        document.getElementById("Admin").style.display = "none"
    }
}
let voorn = "";
let naam = "";
let gebdat = "";

function getAccount() {
    let url = 'https://localhost:44399/Api/Account';
    fetch(url,
        {
            method: "GET",
            headers: {
                'accept': 'application/json',
                'content-Type': 'application/json',
            }
        }).then((response) => {
        if (response.ok) {
            console.log("succes");
            return response.json();

        } else {
            console.log("Something went wrong!");
        }
    })
        .then((response) => {
            let person = JSON.parse(response.data);
            console.log(person);
            voorn = person[sessionStorage.getItem('userid')-4].firstname;
            naam = person[sessionStorage.getItem('userid')-4].name;
            gebdat = person[sessionStorage.getItem('userid')-4].birthdate;
            console.log(voorn, naam, gebdat);

            laadGegevens();
        })}

function showGegevens(){
        document.getElementById('gegevens').style.display = "block";
        document.getElementById('aanpasvelden').style.display = "none";
}
function laadGegevens() {
        document.getElementById("geg_voornaam").innerText = voorn;
        document.getElementById("geg_achternaam").innerHTML = naam;
        document.getElementById("geg_geboortedatum").innerHTML= gebdat;
        document.getElementById("geg_adres").innerHTML="Adres van gebruiker";

        document.getElementById("voornaam").innerHTML=voorn;
        document.getElementById("achternaam").innerHTML=naam;
        document.getElementById("geboortedatum").innerHTML= gebdat;
        document.getElementById("adres").innerHTML="Huidig adres";
        document.getElementById("huisnummer").innerHTML="#";
}
function showAanpasvelden(){
    document.getElementById('gegevens').style.display = "none";
    document.getElementById('aanpasvelden').style.display = "block";
}
function accountupdate() {
    //check elk gegeven of deze aangepast is van het oorspronkelijke, en update de gegevens die aangepast zijn

    document.getElementById("updatemessage").innerHTML="Gegevens zijn succesvol opgeslagen";
}
function changeAccount() {
    let url = 'https://localhost:44399/Api/Account';
    fetch(url,
        {
            method: "GET",
            headers: {
                'accept': 'application/json',
                'content-Type': 'application/json',
            }
        }).then((response) => {
        if (response.ok) {
            console.log("succes");
            return response.json();

        } else {
            console.log("Something went wrong!");
        }
    })
        .then((response) => {
            let person = JSON.parse(response.data);
            console.log(person);
            voorn = person[sessionStorage.getItem('userid')-4].firstname;
            naam = person[sessionStorage.getItem('userid')-4].name;
            gebdat = person[sessionStorage.getItem('userid')-4].birthdate;
            console.log(voorn, naam, gebdat);

            let nieuweVN = document.getElementById('voornaam').value;

            if (voorn != nieuweVN){
                function postRequest() {

                    const firstname = document.getElementById("voornaam").value;
                    const data = {
                        firstName: firstname
                    }

                    if (!checkName(firstname)){
                        return;
                    }
                    fetch('https://localhost:44399/api/Account/', {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(data),
                    }).then(validateResponse)
                        .then(logResult)
                        .then(logData)
                        .catch(logError);
                }
            }

        })}