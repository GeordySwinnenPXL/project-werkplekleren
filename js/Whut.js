function accountupdate() {
    //check elk gegeven of deze aangepast is van het oorspronkelijke, en update de gegevens die aangepast zijn

    document.getElementById("updatemessage").innerHTML="Gegevens zijn succesvol opgeslagen";
}

function passcheck(){
    //var a = document.getElementById("oldpassword").value;
    //if (a!=*wachtwoord in database*) {
    //    document.getElementById("oldpassmessage").innerHTML="Wachtwoord onjuist";
    //    return false;
    //}



    var a = document.getElementById("newpassword").value;
    if (a.length < 6) {
        document.getElementById("newpassmessage").innerHTML="Nieuw wachtwoord moet minsters 8 karakters lang zijn";
        return false;
    }

    var b = document.getElementById("newpassword2").value;
    if (a!=b) {
        document.getElementById("newpassmessage").innerHTML="Wachtwoorden stemmen niet overeen";
        return false;
    }

    document.getElementById("newpassmessage").innerHTML="Wachtwoord opgeslagen";
}

var mandrill = require('node-mandrill')('<your API Key>');

function sendEmail ( _name, _email, _subject, _message) {
    mandrill('/messages/send', {
        message: {
            to: [{email: _email , name: _name}],
            from_email: 'noreply@yourdomain.com',
            subject: _subject,
            text: _message
        }
    }, function(error, response){
        if (error) console.log( error );
        else console.log(response);
    });
}
app.post( '/api/sendemail/', function(req, res){

    var _name = req.body.name;
    var _email = req.body.email;
    var _subject = req.body.subject;
    var _messsage = req.body.message;

    //implement your spam protection or checks.

    sendEmail ( _name, _email, _subject, _message );

});
function sendMail() {
    $.ajax({
        type: 'POST',
        url: 'https://mandrillapp.com/api/1.0/messages/send.json',
        data: {
            'key': 'YOUR API KEY HERE',
            'message': {
                'from_email': 'YOUR@EMAIL.HERE',
                'to': [
                    {
                        'email': 'RECIPIENT@EMAIL.HERE',
                        'type': 'to'
                    }
                ],
                'autotext': 'true',
                'subject': 'Wachtwoordwijziging',
                'html': '<p>Geachte klant,<br/><br/>uw wachtwoord is succesvol veranderd.<br/>Indien u dit niet zelf gedaan heeft, gelieve ons dan te contacteren.<br/><br/>Met vriendelijke groeten<br/>Het Liquify-team</p>'
            }
        }
    })}